package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.files.CsvFileGenerator;
import pl.edu.pwsztar.domain.files.TxtFileGenerator;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.FileService;

import java.io.File;
import java.io.IOException;

@Service
public class FileServiceImpl implements FileService {
    private final TxtFileGenerator txtFileGenerator;
    private final CsvFileGenerator csvFileGenerator;
    private final MovieRepository movieRepository;

    public FileServiceImpl(TxtFileGenerator txtFileGenerator
            , CsvFileGenerator csvFileGenerator
            , MovieRepository movieRepository){
        this.txtFileGenerator = txtFileGenerator;
        this.csvFileGenerator = csvFileGenerator;
        this.movieRepository = movieRepository;
    }

    @Override
    public InputStreamResource returnTxt() throws IOException {
        return txtFileGenerator.toTxt(new FileDto.Builder()
                .file(File.createTempFile("movies",".txt"))
                .movies(movieRepository.findAll(Sort.by(Sort.Direction.DESC,"year")))
                .build());
    }

    @Override
    public InputStreamResource returnCsv() throws IOException {
        return csvFileGenerator.toCsv(new FileDto.Builder()
                .file(File.createTempFile("movies",".csv"))
                .movies(movieRepository.findAll(Sort.by(Sort.Direction.DESC,"year")))
                .build());
    }
}
